[TOC]

# Ejemplo práctico

he creado un ejemplo de fichero hledger llamado `hledger-journal` con los años 2019 y 2020 completos donde podrás ver, cómo se hace:

- modelo 303 (iva trimestral)
- modelo 100 (irpf / renta)
- deducción de iva por material

puedes explorarlo con: `hledger-ui -f hledger-journal`

próximamente también incluiré algunos informes de interés así como por dónde vigilar

# Introducción

Terminología hledger: https://github.com/simonmichael/hledger/wiki/hledger-terminology

Usaremos las subcuentas típicas que aparecen en todos los programas de contabilidad en inglés y a cuáles corresponden en castellano

| Account | Cuenta | Descripción |
| ------- | ------ | ----------- |
| assets | activo | |
| liabilities | pasivo | |
| equity | ? | se usa para mostrar cómo se abren las cuentas |
| revenues | ingresos | entrada de dinero |
| expenses | gastos | |

subcuentas importantes

...?

contrapartida: la suma de todas las operacions anteriores y cambiada de signo


| Subcuentas | Código PGE 2007 | Descripción PGC 2007 |
| ---------- | --------------- | ----------- |
| assets:clients:client1 | 410 | Acreedores por prestaciones de servicios |
| assets:iva21 | 472 | Hacienda Pública, IVA soportado, IVA de facturas recibidas (está en el activo-assets) |
| liabilities:iva21 | 477 | Hacienda Pública, IVA repercutido, IVA de facturas enviadas (está en el pasivo-liabilities) |
| assets:hp:iva | 4700 | cuando resultado `472-477 > 0` sale a cobrar; entonces hp es deudora por IVA |
| liabilities:hp:iva | 4750 | resultado `472-477 < 0` sale a pagar; entonces hp es acreedora por IVA |
| assets:irpf7 | 473 | las retenciones que generamos con nuestra actividad, a compensar con modelo 100 |
| liabilities:hp:irpf | 4751 | para retenciones a subcontrataciones de profesionales autónomos que liquidaremos en modelo 130 |
| assets:cash | 570 | Dinero en caja, en metálico |
| assets:bank1:checking | 572 | Dinero en bancos e instituciones de crédito c/c vista |
| ? | 705 | Prestaciones de servicios |

hp hace referencia a Hacienda Pública

Referencias:

- PGC (Plan General Contabilidad) -> src https://www.boe.es/boe/dias/2007/11/20/pdfs/C00001-00152.pdf
    - más info https://es.wikipedia.org/wiki/Plan_General_de_Contabilidad

# asientos contables

https://en.wikipedia.org/wiki/Accrual -> https://es.wikipedia.org/wiki/Devengo

El Plan General de Contabilidad de España establece el principio del Devengo como uno de los principios obligatorios en el registro contable de ingresos y gastos, estableciéndolo de la siguiente manera: "la imputación de ingresos y gastos deberá hacerse en función de la corriente real de bienes y servicios que los mismos representan y con independencia del momento en que se produzca la corriente monetaria o financiera derivada de ellos".

## facturas emitidas y pagos

### factura

ejemplo:

```
2018/11/18 (2018-1) Factura
  revenues:freelance:cliente1  -1000€
  liabilities:iva21  -210€
  assets:irpf15  150€
  assets:clients:client1
```

- en la subcuenta de ingresos como freelance (autónomo) hay una subcuenta de el cliente, de esta forma sabremos fácilmente qué clientes nos reportan más ganancias
  - estaría bien poder hacer otros tags de subcategorías para saber ganancias por tipo de actividad
- se recauda el IVA para los trimestrales de hacienda
- IPRF, en caso de haber, pasa a la cuenta activos de irpf
- la contrapartida va asociada a la subcuenta de activos, clientes, y el cliente en cuestión

más info:

- https://github.com/simonmichael/hledger/wiki/Common-journal-entries#accrual-basis
- https://github.com/simonmichael/hledger/wiki/Project-accounting

### pago

ejemplo:

```
2018/11/18 (2018-1) Pago
  assets:clients:client1  -1060€
  assets:bank1:checking
```

## facturas recibidas y pagos

### factura recibida

ejemplo:

```
2018/11/29 Factura recibida de producto1
  expenses:compras:provider1  100€
  assets:iva21  21€
  liabilities:providers:provider1
```

### pago

ejemplo:

```
2018/11/30 Pago Factura recibida de producto1
  liabilities:providers:provider1  121€
  assets:bank1:checking
```

# liquidaciones modelo 303

ejemplo si hubiera que hacer liquidación trimestral de los ejemplos anteriores

```
2020/1/30 Liquidación IVA 2019 4T (Modelo 303)
  liabilities:iva21  210€
  assets:iva21  -21€
  liabilities:hp:iva

2020/1/30 Pago modelo 303
  liabilities:hp:iva  189€
  assets:bank:checking
```
