la renta también es un buen momento para hacer balance del año, y recomprobar gastos. este comando fue útil:

    hledger -f /path/to/hledger.journal incomestatement -p 2021

entrar en https://www.agenciatributaria.es/AEAT.internet/Renta.shtml

no incorporar lo que sale como autónomo, porque tal como dice:

> NOTA: Solamente en el caso de ser rendimientos del trabajo se trasladarán a las casillas, 3 (página 4) y 596 (página 21). En el caso de tratarse de rendimientos de actividades económicas pulse cancelar y marque NO Trasladar, el contribuyente deberá reflejarlos en el programa de forma manual (páginas, 8, 9 o 10).

vamos a D1 (página 7)

- 0166 A05 profesionales
- 0168 N estimación directa normal (no simplificada)
- 0167 actividad IAE
- 0171 Ingresos de explotación: **Importante**. Al pulsar editar se abre una ventana, se pone los correspondientes *Ingresos de explotación* (base imponible de las facturas emitidas) y la retención total anual de IRPF en *Retenciones a cuenta del impuesto relativas a la actividad (salvo por arrendamiento de inmuebles urbanos que tengan la consideración de actividad económica)*
  - en mi caso tengo varias fuentes de revenues, entonces quiero `revenues:freelance` y el resultado de este comando me hace la suma de esa parte `hledger --depth 2 -f /path/to/hledger.journal incomestatement -p 2021`
  - para las retenciones, el 15% de este ingreso
- 0186: deducciones: cuota seguridad social

- Otros conceptos fiscalmente deducibles. https://sede.agenciatributaria.gob.es/Sede/ayuda/manuales-videos-folletos/manuales-ayuda-presentacion/irpf-2021/7-cumplimentacion-irpf/7_4-rendimientos-actividades-economicas/7_4_2-regimen-estimacion-directa/7_4_2_3-gastos-fiscalmente-deducibles/otros-conceptos-fiscalmente-deducibles.html
- https://sede.agenciatributaria.gob.es/Sede/ayuda/manuales-videos-folletos/manuales-ayuda-presentacion/irpf-2021/7-cumplimentacion-irpf/7_4-rendimientos-actividades-economicas/7_4_2-regimen-estimacion-directa/7_4_2_3-gastos-fiscalmente-deducibles/servicios-profesionales-independientes.html
