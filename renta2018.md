para calcular la renta fue muy útil este comando:

    hledger -f /path/to/hledger.journal incomestatement -p 2019

notas sobre el formulario de la renta

- página 4
  - Declaración complementaria
  - Solicitud de rectificación de autoliquidación: Si inicia un procedimiento de rectificación de autoliquidación, por resultar una cantidad a devolver mayor a la solicitada, o una cantidad a ingresar menor, indíquelo marcando esta casilla (también si el resultado no varia)
    - marcar esta opción si en una modificación posterior de la declaración de la renta te sale que te has podido deducir más gastos y que por lo tanto se puede reducir el ingreso a hacienda
      - si en el momento de la rectificación hacienda ya ha hecho cargo en banco o la has pagado tienes que pedir cita previa sobre IRFP con motivo de "devolución pago indebido". En Barcelona 912 901 340 (Poblenou). Parece que es este trámite, "Devolución de ingresos indebidos" https://www.agenciatributaria.gob.es/AEAT.sede/procedimientoini/RA03.shtml
        - otro sitio donde reclamar el ingreso indebido es en cita previa de agencia tributaria. recaudación -> otros trámites de recaudación
- página 5 Rendimientos del trabajo - aquí debería salir automáticamente lo que corresponde a contratos laborales
- página 9 Rendimientos de actividades económicas en estimación directa - aquí tienes que introducir manualmente la facturación como autóno profesional
  - 0111 Tipo de actividad/es realizada/s clave indicativa: 5 Restantes actividades profesionales
  - 0113 Modalidad aplicable del método de estimación directa: N estimación directa normal
  - 0116 Ingresos de explotación: **aquí se pone la retención IRPF**. Al pulsar editar se abre una ventana, se pone los correspondientes *Ingresos de explotación* (base imponible de las facturas emitidas) y la retención total anual de IRPF en *Retenciones a cuenta del impuesto relativas a la actividad (salvo por arrendamiento de inmuebles urbanos que tengan la consideración de actividad económica)*
    - verificación: página 32 debería aparecer lo del IRPF en *Por rendimientos de actividades económicas* casilla 0599
  - 0123 Seguridad Social a cargo de la empresa (incluidas las cotizaciones del titular): aquí yo puse las cuotas mensuales de la seguridad social de autónomo pero me dijeron que podría no ser muy correcto (entonces dónde irían?)
  - 0124 Otros gastos de personal: - aquí te puedes deducir la cuota de colegio profesional
  - 0129 Suministros (entre otros agua, gas, electricidad, telefonía, internet): seguro que algo te puedes deducir aquí
- página 39 Deducciones por donativos y otras aportaciones
- página 47 Deducciones autonómicas

entendiendo cómo hacer el asiento contable del modelo 100

SEGUNDA PARTE. NORMAS DE REGISTRO Y VALORACIÓN. 13 Impuestos sobre beneficios. 5 Empresarios individuales: En el caso de empresarios individuales no deberá lucir ningún importe en la rúbrica correspondiente al impuesto sobre beneficios. A estos efectos, al final del ejercicio las retenciones soportadas y los pagos fraccionados del Impuesto sobre la Renta de las Personas Físicas deberán ser objeto del correspondiente traspaso a la cuenta del titular de la empresa. (se refiere a la cuenta del *titular de la explotación (550)*
