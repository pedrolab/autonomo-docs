[TOC]

se recomienda certificado de persona física para facilitar trámites con la administración https://www.sede.fnmt.gob.es/certificados/persona-fisica/obtener-certificado-software

## Alta inicial

Para la gestión de los trámites necesarios es recomendable tener un certificado digital de la FNMT (Fábrica Nacional Moneda Timbre), que nos permitirá a cualquier hora realizar las gestiones necesarias https://www.sede.fnmt.gob.es/certificados/persona-fisica

Hay dos altas que realizar:

- hacienda: donde se harán hacen los modelos 100 y 303
  - https://www.agenciatributaria.gob.es/AEAT.sede/tramitacion/G322.shtml
- Alta en seguridad social (RETA - Régimen Especial de Trabajadores por Cuenta Propia o Autónomos): cuota mensual a pagar, nos da coberturas sociales en caso de baja, paro, vacaciones, etc.
  - [link largo](https://sede.seg-social.gob.es/wps/portal/sede/sede/Ciudadanos/CiudadanoDetalle/!ut/p/z0/fY7LDoIwEEV_hQ1LMy0YkCUxhqCSaAyhdmOaUnAUy6No_HyLe1ieeydzLnBgwLX4YC1GbLVoLF95cPNpsKYRoceEpDsS59khj_yzn6QULsrAHvjCkRdMX7wh22Y18E6M9xXqqgUmKmxQSCtylIPayAG7iYCRcLZbdtlB1oWPvucxcNnqUX1HYEaV6vYnjWVrXDIFLpH4LkUp9JTMCV3ikWATRtA9k-L0KuIfBMV4ow!!/) o con su buscador favorito buscar por *Alta Reta*
    - cuidado porque en el alta de RETA en algún momento nos pide un código de CNAE, podemos hacer la conversión respecto el código que pusimos en hacienda / IAE aquí por ejemplo: https://www.iberaval.es/conversor/

## Calendario y Entrega de Modelos

### Modelo 303

[IVA trimestral (Modelo 303)](https://sede.agenciatributaria.gob.es/Sede/procedimientoini/G414.shtml) - [instrucciones](https://infoautonomos.eleconomista.es/fiscalidad/modelo-303-iva/):

- 1T 04-01--04-20
- 2T 07-01--07-20
- 3T 10-01--10-20
- 4T 01-01--01-30
    - en el periodo de entrega de 4T también se entrega el [IVA anual (Modelo 390)](https://sede.agenciatributaria.gob.es/Sede/procedimientoini/G412.shtml) - [instrucciones](https://infoautonomos.eleconomista.es/fiscalidad/modelo-390-el-resumen-anual-de-iva/) - que es solo una entrega informativa

para pagar 303 mirar para el año y cuatrimestre requerido las cuentas de IVA de Liabilities

    hledger -f /path/to/hledger.journal bs -p 2019 -Q

o dinámicamente con `hledger-ui -f /path/to/hledger.journal` vamos a la subcuenta de liabilities y hacemos "shift + flecha abajo" 2 veces, nos saldrá un ficho por fecha de cuatrimestre y nos dirá 2019qN (N en el cuatrimestre más cercano), podemos movernos con los diferentes cuatrimestres con "shift + flechas izquierda-derecha"

[para pagar los IVA trimestrales](https://www.agenciatributaria.es/AEAT.internet/Inicio/Ayuda/_comp_Consultas_informaticas/Categorias/Pago_de_impuestos__deudas_y_tasas/Impuestos/Pago_de_autoliquidaciones__obtencion_de_un_NRC_/Pago_de_autoliquidaciones__obtencion_de_un_NRC_.shtml) hace falta un código que nos dará el banco si lo hacemos los últimos días. Si lo hacemos los primeros días se puede domiciliar.

### Modelo 390

declaración informativa

https://declarando.es/modelo-390/casillas-95-99-y-105

- https://vidraassociats.com/es/vidranews/como-rectificar-declaraciones-informativas-erroneas
- https://www.mieconomista.eu/presentar-la-declaracion-trimestral-del-iva-fuera-de-plazo
- https://www.supercontable.com/pag/documentos/comentarios/rectificacion_declaraciones_informativas.html

### Renta (Modelo 100)

Declaración de la renta e IRPF (Modelo X): 07-30

- [Tramos IRP autónomos](https://www.blueindic.com/blog/tramos-del-irpf-para-el-autonomo-en-2018/)

### Otros modelos

- **Modelo 130**: Los autónomos que realicen una actividad profesional, salvo que al menos el 70% de sus ingresos procedentes de la actividad profesional hubieran tenido retención o ingreso a cuenta en el ejercicio anterior. Es decir, **están exentos del modelo 130 si han facturado con retenciones más del 70% de su facturación a empresas, autónomos y entidades jurídicas con sede en España**
  - https://infoautonomos.eleconomista.es/fiscalidad/modelo-130-irpf-autonomos/
  - modelo 130 no tiene resumen anual como el modelo 390 https://ayuda.declarando.es/es/articles/1079822-el-modelo-130-no-tiene-resumen-anual
- **Modelo 347** no hace falta hacerlo si generas todas las facturas como profesional (y su retención)
  - https://infoautonomos.eleconomista.es/fiscalidad/modelo-347-declaracion-de-operaciones-con-terceras-personas/#comment-3694352006
  - https://cuentica.com/asesoria/modelo-347-declaracion-anual-de-operaciones-con-terceros/

## Buscador AEAT

https://sede.agenciatributaria.gob.es/Sede/search.html

## Manuales prácticos

https://sede.agenciatributaria.gob.es/Sede/manuales-practicos.html

## Notas adicionales

- Entregas tardías se aplica multa que hace aumentar el pago pendiente en un pago de +20% e intereses
- Aunque no haya facturación en dicho trimestre, hay que entregarlo a 0 (si se entrega tarde no hay multa)
- Exención de IVA en Europa, registrarse en ROI https://www.txerpa.com/blog/2014/09/16/como-hacerse-autonomo-con-actividades-intracomunitarias
- [Renta Web](https://www.lavozdegalicia.es/noticia/economia/2017/05/07/autonomos-saben-irpf/0003_201705G7P37991.htm)
- [teléfonos de contacto](https://www.agenciatributaria.gob.es/AEAT.sede/Inicio/_otros_/_Direcciones_y_telefonos_/Informacion_telefonica/Informacion_telefonica.shtml)
- ayuda oficial declaración renta / modelo 100 https://www.agenciatributaria.es/AEAT.internet/Ayuda/18Presentacion/100.shtml
- [Cuándo te interesa aplicar la retención reducida del IRPF](https://infoautonomos.eleconomista.es/blog/retencion-reducida-del-irpf-nuevos-autonomos/)
- ser autónomo sin darse de alta https://asepyme.com/alta-de-autonomo-con-pocos-ingresos/
- Facturas sin IVA
    - https://www.serautonomo.net/dudas/como-debo-hacer-facturas-sin-iva
    - https://www.serautonomo.net/actividades-exentas-de-iva.html
- Un autónomo puede incorporar equipos en forma de suplidos https://infoautonomos.eleconomista.es/facturas/como-facturar-suplidos-y-gastos-reembolsables/
- [agreement-for-supply-services.md]() es un documento de ejemplo (donado por un compañero) para estabilizar la relación con un cliente, para generar el PDF recomiendo: `sudo apt install pandoc wkhtmltopdf` y procesar el markdown `pandoc -s agreement-for-supply-services.md -o agreement.pdf --pdf-engine=wkhtmltopdf`
- [Una sentencia establece que no es necesaria una factura para devengarse un gasto](https://translogia.es/una-sentencia-establece-que-no-es-necesaria-una-factura-para-devengarse-un-gasto/)
- a cuánto se paga el km https://www.declaree.com/es/a-cuanto-se-paga-el-kilometro-en-espana-en-2019/
- [He recibido una sanción por no presentar un impuesto cuando no debía ¿cómo proceder?](https://getquipu.com/blog/he-recibido-una-sancion-por-no-presentar-un-impuesto-cuando-no-debia-como-proceder/#no-debo-presentar-un-impuesto-pero-he-recibido-una-sancion-de-la-eaet)
- [473 vs 4751](https://www.slideshare.net/TAREIXAT/ctas-de-retenciones-473-y-4751-para-slideshare-68143810)
- cuota autónomo desgrava https://getquipu.com/blog/puedo-anadir-cuota-tarifa-autonomos-como-gasto/
