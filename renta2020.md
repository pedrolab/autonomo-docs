la renta también es un buen momento para hacer balance del año, y recomprobar gastos. este comando fue útil:

    hledger -f /path/to/hledger.journal incomestatement -p 2020

entrar en https://www.agenciatributaria.es/AEAT.internet/Renta.shtml

click a *Servicio de tramitación borrador / declaración (Renta Web)*

actividades económicas: no incorporar ningún registro para este concepto, pero el número debería ser el mismo (tal como dice la ayuda: NOTA: Solamente en el caso de ser rendimientos del trabajo se trasladarán a las casillas, 3 (página 4) y 596 (página 22). En el caso de tratarse de rendimientos de actividades económicas pulse cancelar y marque NO Trasladar, el contribuyente deberá reflejarlos en el programa de forma manual (páginas, 8, 9 o 10).)

- D1 rendimientos de actividades económicas en estimación directa
  - 0166 tipo de actividad: 5 (para profesionales)
  - 0167 IAE que corresponda
  - 0171 Ingresos de explotación: **Importante**. Al pulsar editar se abre una ventana, se pone los correspondientes *Ingresos de explotación* (base imponible de las facturas emitidas) y la retención total anual de IRPF en *Retenciones a cuenta del impuesto relativas a la actividad (salvo por arrendamiento de inmuebles urbanos que tengan la consideración de actividad económica)*
  - nota: base imponible facturas emitidas es el revenues de `hledger -f org/hledger.journal incomestatement -p 2020`
  - nota: irpf 2020 de subcuenta `assets:irpf15` para 2020
  - 186 poner los gastos de seguridad social


https://www.agenciatributaria.es/AEAT.internet/Inicio/Ayuda/Manuales__Folletos_y_Videos/Manuales_de_ayuda_a_la_presentacion/Ejercicio_2018/_Ayuda_Modelo_100/7__Cumplimentacion_IRPF__I_/7_5__Rendimientos_de_actividades_economicas/7_5_2__Regimen_de_estimacion_directa/7_5_2_3__Gastos_fiscalmente_deducibles/7_5_2_3__Gastos_fiscalmente_deducibles.html

- 191: gastos dietas contribuyente teniendo en cuenta los límites


deducciones:

Capítulo 7. Rendimientos de actividades económicas. Método de estimación directa -> Gastos fiscalmente deducibles

especialmente interesante los "gastos del titular de la actividad"



lo que ocurrió en 2019 se declara en 2020, no puedes hacerlo en 2021
