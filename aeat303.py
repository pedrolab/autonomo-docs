#!/usr/bin/env python3
#
# Copyright 2022 -- Evilham <contact@evilham.com
# Intended license: BSD 2-clause
#


import os
from datetime import datetime, timedelta
from io import StringIO

import click

# This uses the AEAT documentation at
#  https://sede.agenciatributaria.gob.es/Sede/ayuda/disenos-registro/modelos-300-399.html

SERVICE_VAT_RATE = 21


def s_len(s):
    position = s.tell()
    s.seek(0, os.SEEK_END)
    size = s.tell()
    s.seek(position, os.SEEK_SET)
    return size


def gen303_head(sw_version, sw_nif, year, period):
    assert len(sw_version) == 4, "Dev version must be 4 chars long"
    assert len(sw_nif) == 9, "Dev NIF must be 9 chars long"
    s = StringIO()
    s.write("<T")
    s.write("303")
    s.write("0")
    s.write(f"{year}")
    s.write(f"{period}T")
    s.write(f"0000>")
    s.write("<AUX>")
    s.write(" " * 70)
    s.write(sw_version)
    s.write(" " * 4)
    s.write(sw_nif)
    s.write(" " * 213)
    s.write("</AUX>")
    assert s_len(s) == 328, "Wrong header"
    return s.getvalue()


def gen303_tail(year, period):
    s = StringIO()
    s.write(f"</T3030{year}{period}T0000>")
    return s.getvalue()


def gen303_01(year, period, nif, name, rg_i_base, rg_received, rg_o_base, rg_spent):
    s = StringIO()
    # Nota 1:
    # U: Domiciliacion ingreso en CCC
    # C: Solicitud de compensación
    # D: Devolución
    # G: Cuenta corriente tributaria ingreso
    # I: Ingreso
    # N: Sin actividad / resultado cero
    # V: Cuenta corriente tributaria devolución
    # X: Devolución por transferencia al extranjero (3T, 4T y 07 a 12)
    PI = "U"
    s.write("<T")
    s.write("303")
    s.write("01000")
    s.write(">")
    s.write(" ")
    s.write(PI)
    s.write(nif)
    s.write(name)
    s.write(" " * (80 - len(name)))
    s.write(f"{year}")
    s.write(f"{period}T")
    # Foral? 1: Sí, 2: No
    s.write("2")
    # Devolución mensual / RIVA? 2: no
    s.write("2")
    # Exclusivamente regimen simplificado? 3: No (sólo RG)
    s.write("3")
    # Preguntas forales (1: sí, 2: no):
    # - Autoliquidación conjunta
    # - Régimen especial del criterio de caja
    # - Destinatario de operaciones al reg. especial crit. caja
    # - Prorrata especial
    # - Revocación prorrata especial
    # - Concurso acreedores
    s.write("2" * 6)
    # Fecha concurso (vacío)
    s.write("0" * 8)
    # Auto de declaración 1: preconcurso, 2 post, blanco: NO
    s.write(" ")
    # Acogido SII (1: sí, 2: no)
    s.write("2")
    # Excepto 390
    if period != 4:
        s.write("0")
    else:
        # 1: sí, 2: no
        s.write("2")
    # Volumen anual != 0 (LIVA): 0 si no exonerado
    s.write("0")
    # Casillas régimen general 01 - 06:
    assert s_len(s) == 129, "Wrong position before 30301 - [01]"
    s.write(str(rg_i_base).zfill(17))
    s.write(f"{SERVICE_VAT_RATE*100}".zfill(5))  # 21% VAT
    s.write(str(rg_received).zfill(17))
    for i, casilla in enumerate(["04", "07"]):
        assert s_len(s) == 129 + (i + 1) * (
            17 + 5 + 17
        ), f"Wrong position before 30301 - [{casilla}]"
        s.write("0" * (17 + 5 + 17))
    assert rg_received == round(
        SERVICE_VAT_RATE / 100.0 * rg_i_base
    ), f"Bad VAT amount: {rg_received}"
    # Casillas 10 - 26
    assert s_len(s) == 246, "Wrong position before 30301 - [10]"
    s.write("0" * 253)
    # Casilla 27: Total
    s.write(str(rg_received).zfill(17))
    # Casilla 28: Operaciones corrientes
    s.write(str(rg_o_base).zfill(17))
    # Casilla 29: cuota op. corrientes
    s.write(str(rg_spent).zfill(17))
    # Casillas 30-44
    s.write("0" * 255)
    # Casilla 45: Total
    s.write(str(rg_spent).zfill(17))
    # Casilla 46: Resultado
    s.write(str(rg_received - rg_spent).zfill(17))
    # Reservado aeat
    s.write(" " * 600)
    s.write(" " * 13)
    s.write("</T30301000>")
    assert s_len(s) == (1453 + 12 - 1), "Wrong page 30301"
    return s.getvalue()


# We are not using page 2
# def gen303_02(...)


def gen303_03(rg_received, rg_spent, iban):
    s = StringIO()
    s.write("<T")
    s.write("303")
    s.write("03000")
    s.write(">")
    # Casillas 59, 60, 120, 122, 123 y 124, 62, 63, 74, 75, 76
    s.write("0" * (11 * 17))
    res = rg_received - rg_spent
    # Casilla 64, total
    s.write(str(res).zfill(17))
    # 100% al estado
    s.write("10000")
    assert s_len(s) == 220, "Wrong length before 30303 - [66]"
    # Casilla 66
    s.write(str(res).zfill(17))
    # Casillas 77, 110, 78, 87, 68
    s.write("0" * (5 * 17))
    # Casilla 69
    s.write(str(res).zfill(17))
    # Casilla 70
    s.write("0" * 17)
    # Casilla 71, resultado liquidación
    s.write(str(res).zfill(17))
    assert s_len(s) == 373, "Wrong length after 30303 - [71]"
    s.write(" " * (1 + 13 + 1 + 11))
    assert s_len(s) == 399, "Wrong length before 30303 - IBAN"
    s.write(iban)
    s.write(" " * (34 - len(iban)))
    assert s_len(s) == 433, "Wrong length after 30303 - IBAN"
    s.write(" " * (17 + 70 + 35 + 30 + 2))
    # Devolución marca SEPA
    #   0: Vacía (no devolución)
    s.write("0")
    s.write(" " * 600)
    s.write("</T30303000>")
    assert s_len(s) == (1189 + 12 - 1), "Wrong page 30303"
    return s.getvalue()


def gen303_04(year, period):
    s = StringIO()
    if period != 4:
        # Only in last term
        return s.getvalue()
    s.write("<T")
    s.write("303")
    s.write("04000")
    s.write(">")
    s.write(" ")
    # Field N. 6-18: Only last term
    s.write(" " * 43)
    # Casillas 89, 90, 91, 92, 107
    s.write("0" * 25)
    # Casillas 80, 81, 93, 94, 83, 84, 125, 126, 127, 128, 86, 95, 96, 97, 98, 79, 99, 88
    s.write("0" * (18 * 17))
    s.write(" " * 600)
    s.write("</T30304000>")
    assert s_len(s) == (987 + 12 - 1), "Wrong page 30304"
    return s.getvalue()


def gen303(
    sw_version,
    sw_nif,
    year,
    period,
    nif,
    name,
    rg_i_base,
    rg_received,
    rg_o_base,
    rg_spent,
    iban,
):
    assert len(name) <= 80, "Name may not be longer than 80 characters"
    s = StringIO()
    s.write(gen303_head(sw_version, sw_nif, year, period))
    s.write(
        gen303_01(year, period, nif, name, rg_i_base, rg_received, rg_o_base, rg_spent)
    )
    s.write(gen303_03(rg_received, rg_spent, iban))
    s.write(gen303_04(year, period))
    s.write(gen303_tail(year, period))
    return s.getvalue()


_presentation_delta = timedelta(days=20)
_date_in_period = datetime.now() - _presentation_delta


@click.command()
@click.option(
    "--dev-version",
    default=str(_date_in_period.year)[2:] + str(_date_in_period.month).zfill(2),
    help="The version for the tool",
)
@click.option(
    "--dev-nif", prompt=True, required=True, help="The NIF of the tool developer"
)
@click.option(
    "--year",
    default=_date_in_period.year,
    help="Year for which you are presenting Model 303. Defaults to the year 20 days ago",
)
@click.option(
    "--period",
    default=1 + int((_date_in_period.month-1) / 3),
    help="Three-month period for which you are presenting Model 303. Defaults to the three-month period 20 days ago",
)
@click.option(
    "--nif", prompt=True, required=True, help="Your NIF. Defaults to env var AEAT_NIF"
)
@click.option(
    "--legal-name",
    prompt=True,
    required=True,
    help="Your legal name in format: 'LASTNAME1 LASTNAME2, NAME1 NAME2 ...' (max 80 chars). Defaults to env var AEAT_LEGAL_NAME",
)
@click.option(
    "--iban",
    prompt=True,
    required=True,
    help="Where AEAT should charge VAT from; you must be beneficiary. Defaults to env var AEAT_IBAN",
)
@click.option(
    "--received-base",
    prompt=True,
    required=True,
    type=click.INT,
    help="The base amount of services provided in the period. Defaults to env var AEAT_RECEIVED_BASE",
)
@click.option(
    "--received-vat",
    prompt=True,
    required=True,
    type=click.INT,
    help="The total VAT received for services provided in the period. Defaults to env var AEAT_RECEIVED_VAT",
)
@click.option(
    "--spent-base",
    default=0,
    help="The base amount of services/goods consumed in the period. Defaults to env var AEAT_SPENT_BASE",
)
@click.option(
    "--spent-vat",
    default=0,
    help="The total VAT paid for services/goods in the period. Defaults to env var AEAT_SPENT_VAT",
)
def gen303cmd(
    dev_version,
    dev_nif,
    year,
    period,
    nif,
    legal_name,
    iban,
    received_base,
    received_vat,
    spent_base,
    spent_vat,
):
    m = gen303(
        dev_version,
        dev_nif,
        year,
        period,
        nif,
        legal_name,
        received_base,
        received_vat,
        spent_base,
        spent_vat,
        iban,
    )
    print(m, end="")


if __name__ == "__main__":
    gen303cmd(auto_envvar_prefix="AEAT")
