# AGREEMENT FOR THE SUPPLY OF SERVICES

DATED on the <date>

BETWEEN

<enterprise name>

AND

<provider name>

THE PARTIES

On the one hand, **<enterprise name>**, the client, having its registered offices at <enterprise address>, with VAT number <enterprise VAT number> hereby lawfully represented by Mr. <enterprise managing director>, hereafter called the **ENTERPRISE**.

AND

On the other hand Mr **<provider name>**, the service provider, having its registered offices at <provider address>, hereafter called **PROVIDER**.

It is hereby agreed by and between the parties that PROVIDER shall provide the services specified in this Agreement to ENTERPRISE, subject to the following terms and conditions.

## 1 Nature of the agreement

PROVIDER shall on behalf of ENTERPRISE provide such services as may be designated by ENTERPRISE in the field of *software engineering* with the role of *Software Developer* to ENTERPRISE or ENTERPRISE Clients.

## 2 Duration

This contract shall take effect on the <contract start date> and will remain valid for an unspecified period of time, until any of the points in the section "***7 Cessation of Agreement***" applies.

## 3 Provision of Services

3.1. The services shall be provided at PROVIDER’s premises or at ENTERPRISE’s Client premises or at ENTERPRISE’s premises unless otherwise determined in writing.

3.2. PROVIDER is required to submit weekly timesheets into an on-line system designated by ENTERPRISE.  Filings for the weekly timesheets are required by each Monday night for the complete previous week.

3.3. The hourly rate of pay, exclusive of VAT, for ENTERPRISE to pay to PROVIDER in return for the services provided is set to <hourly rate> EURO/h.

3.4. The expected regular hourly input per week  is set to  <hours per week> h/week. Hourly input per week of more than or less than 8 hours in relation to the expected regular hourly input per week shall always need to be approved up front by both parties.

3.5 Force majeure: If a party is obstructed in performing any of its obligations by an event outside its reasonable control, then this Agreement shall remain partially of fully suspended throughout the duration thereof, each party being exempt from any claims for damages arising there from. Either of the parties thus affected shall inform the other in writing of same and furnish evidence as appropriate.

## 4 Intellectual property

4.1. The copyright and any other intellectual or industrial property rights on whatever system, program, instructions and reports, together with any results and documentation obtained and information contained therein, arising from this agreement or the relevant Task Order in question, shall be held by ENTERPRISE or be assigned to a third party appointment by ENTERPRISE.

4.2. PROVIDER declares his willingness to co-operate insofar as is necessary in the assignment of the above rights. PROVIDER shall not obstruct any future use and/or development of such transferred rights.

## 5 Fees

5.1. The fees payable to PROVIDER shall be in accordance with the rates set out in the “***3 Provision of Services***” section to this Agreement.

5.2. The fees can, on top, include expenses such as travel and accommodation costs to and from PROVIDER premises to ENTERPRISE premises or ENTERPRISE’s client sites, which need to be approved up front by ENTERPRISE. Any other expenses always need to be approved up front by ENTERPRISE.

5.3. The fees provided for in this Agreement are exclusive of VAT.

## 6 Invoicing

6.1. PROVIDER shall invoice on a monthly basis. Invoices can only be processed in combination with the monthly project activity reports and/or timesheets endorsed as appropriate by ENTERPRISE representative.

6.2. ENTERPRISE will ensure invoices are paid within a period of 30 days of their receipt date and activity reports as endorsed by ENTERPRISE representative.

6.3. Payment shall be issued upon receipt of invoices by ENTERPRISE.

6.4. ENTERPRISE shall be entitled to deduct from the invoiced sums all payments owed by PROVIDER for reasons related to the agreement.

## 7 Cessation of Agreement

7.1. Each of the parties shall be entitled to terminate this Agreement in the following cases:

a. Each of the parties shall be entitled as of right to terminate forthwith this Agreement prematurely and without further proof of default, either in whole or in part and with immediate effect in the event of either party declaring suspensions of payments where the other party is declared bankrupt.

b. ENTERPRISE shall be entitled as of right to terminate forthwith this Agreement prematurely and without further proof of default, either in whole or in part and with immediate effect where the misconduct or serious breach trust of PROVIDER has been proven.

7.2. Moreover, each of the parties shall be entitled as of right to terminate forthwith this Agreement prematurely and without further proof of default, either in whole or in part and with immediate effect, in the event of non-compliance by either of the parties with any outstanding obligations arising from the agreement and the fulfillment thereof, where such non-compliance is shown to be of such a serious nature that the agreement cannot reasonably be claimed to have been complied with, or due to failure in fulfilling these obligations or in taking appropriate steps to rectify matters.

7.3 Each of the parties shall be entitled to terminate this Agreement by giving a three month notice period.

7.4. Every decision of premature cessation of this Agreement has to be notified by registered letter to the other party or by mutual consent agreement, signed by both parties.

## 8 Applicable law and disputes

8.1. This Agreement is subject to <country/region> state law.

8.2. Any dispute concerning the establishment, interpretation or performance of this Agreement of other agreements arising there from, together with any other dispute in relation to or concerning this Agreement, be it legal or factual or both, shall be submitted for arbitration to the relevant adjudicator at the competent <country/region> court.

## 9 Miscellaneous

9.1 PROVIDER shall obtain at the earliest possible time a EU VAT ID in order to apply the “reverse charge procedure” for intra-EU delivery of services, so his invoices to ENTERPRISE are not subject to <provider_country> VAT.

9.2 Any amendment or modification to this agreement requires written form and signature.

Date, Place:

    <enterprise name>

    <enterprise  director name> (Managing Director)

Date, Place:

    <provider name>
